module Paths_cabal_install (
    version,
  ) where

import Data.Version (Version(..))

version :: Version
version = Version {versionBranch = [1,23,0,0], versionTags = []}
